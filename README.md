# Furion

#### 介绍

.NET 5 企业级应用开发框架。

#### 项目情况

已捐赠给 [dotNET China](https://gitee.com/dotnetchina) 

#### 项目传送门

[Furion](https://gitee.com/dotnetchina/Furion)
